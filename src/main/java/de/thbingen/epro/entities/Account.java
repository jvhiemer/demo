package de.thbingen.epro.entities;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.Getter;

@Getter
@Entity(name = "accounts")
public class Account {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "user_id")
    private long id;

    private String username;

    private String password;

    private String email;

    private Date createdOn;

    @ManyToMany
    @JoinTable(name = "account_roles",
        joinColumns = @JoinColumn(name = "role_id"),
        inverseJoinColumns =  @JoinColumn(name = "user_id"))
    private Set<Role> roles;

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasword() {
        return this.password;
    }
}
