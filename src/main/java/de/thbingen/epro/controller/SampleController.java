package de.thbingen.epro.controller;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import de.thbingen.epro.entities.Account;
import de.thbingen.epro.repositories.AccountRepository;

@RestController
public class SampleController {

    private AccountRepository accountRepository;

    public SampleController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    /**
     * POST
     * Headers:
     *  * Accept: application/json -> RequestHeader
     *  * Content-Type: application/json
     * http://domain.com/accounts/1 (id -> PathVariable)?name=test (name -> RequestParam)
     * Body:
     * {
     *  "username": "bla",
     *  "password": "blue"
     * } -> RequestBody
     * @return
     */
    @PostMapping( value = "/accounts", consumes = {"application/json"})
    @ResponseBody List<Account> home(@PathVariable long id, @RequestParam String name, 
        @RequestHeader HttpHeaders httpHeaders, @RequestBody Account account) {
        List<Account> accounts = accountRepository.findAll();
        return accounts;
    }

    @PutMapping(value = "/accountssss")
    ModelAndView home2() {
        List<Account> accounts = accountRepository.findAll();

        ModelAndView mav = new ModelAndView();
        mav.setViewName("accounts");
        mav.addObject("accounts", accounts);
        return mav;
    }

    /**
     * Client -> Server
     * 
     * Headers
     *  * Method: Post/Put
     *  * Accept: application/json 
     * Body
     * 
     * Server -> Client
     * Headers
     *  * Response Code: ...
     *  * Context-Type: application/json
     * Body
     */
    
}
