package de.thbingen.epro;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.thbingen.epro.controller.SampleController;

/**
 * Unit test for simple App.
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = SampleController.class)
class SampleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void whenWorking_thenReturns200() throws Exception {
        mockMvc.perform(post("/sample")
        .contentType("application/json"))
        .andExpect(status().isOk());
    }

}